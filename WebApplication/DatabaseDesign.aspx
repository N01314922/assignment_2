﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DatabaseDesign.aspx.cs" Inherits="WebApplication.DatabaseDesign" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Data Manipulation Language(DML)</h2>
    <p>Data manipulation language (DML) statements access and manipulate data in existing tables.</p>
    <p>In the SQL Developer environment, you can enter a DML statement in the Worksheet. Alternatively, 
        you can use the SQL Developer Connections frame and tools to access and manipulate data.</p>
    <p>To see the effect of a DML statement in SQL Developer, you might have to select the 
        schema object type of the changed object in the Connections frame and then click the Refresh icon.</p>
    <h4>About the INSERT Statement</h4>
     <p>The INSERT statement inserts rows into an existing table.
        The simplest recommended form of the INSERT statement has this syntax:
        INSERT INTO table_name (list_of_columns)
        VALUES (list_of_values);</p>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="sidebar1" runat="server">
    <h2>Links</h2>
    <p><a href="https://docs.oracle.com/database/121/TDDDG/tdddg_dml.htm#TDDDG23000">Oracle</a></p>
    <p><a href="https://stackoverflow.com/questions/2578194/what-is-ddl-and-dml">Stack Overflow </a></p>
    <p><a href="https://en.wikipedia.org/wiki/Data_manipulation_language"> Wikipedia </a></p>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="sidebar2" runat="server">
    <h2>Example</h2>

    <p>INSERT INTO EMPLOYEES (
           EMPLOYEE_ID,
           FIRST_NAME,
           LAST_NAME,
           EMAIL,
           PHONE_NUMBER
        )
  </p>
   <p>
        VALUES (
          10,              
          'George',        
          'Gordon',        
          'GGORDON',       
          '650.506.2222'
           );
    </p>

    <h4>Result:<h4>
        <p>1 row created.</p>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="footer" runat="server">
    <h2>Notes</h2>

    <p>Every column in list_of_columns must have a valid value in the corresponding position in list_of_values. </p>
    <p>Therefore, before you insert a row into a table, you must know what columns the table has, and what their valid values are. </p>
</asp:Content>
