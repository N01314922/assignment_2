﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DegitalDesign.aspx.cs" Inherits="WebApplication.DegitalDesign" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h2>HTML Forms</h2>
    <p>The HTML form element defines a form that is used to collect user input.</p>
    <p>An HTML form contains form elements.</p>
    <p> Form elements are different types of input elements, like text fields, checkboxes, radio buttons, submit buttons, and more.</p>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="sidebar1" runat="server">
 
    <h2>Links</h2>
    <p><a href="https://www.w3schools.com/html/html_forms.asp">W3Schools</a></p>
    <p><a href="https://www.semrush.com/blog/semantic-html5-guide/">Semrush</a></p>
    <p><a href="https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/Using_HTML_sections_and_outlines">Developrs.mozilla</a></p>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="sidebar2" runat="server">
           <h2>Example</h2>

            <p> &lt;!DOCTYPE html&gt;</p>
            <p> &lt;html&gt;</p>
            <p> &lt;body&gt;</p>
            <p> &lt;h4&gt;Text Input&lt;/h4&gt;</p>
            <p> &lt;form&gt;</p>
            <p> First name:</p>
            <p> &lt;input type="text" name="firstname"&gt;</p>
            <p> Last name:</p>
            <p> &lt;input type="text" name="lastname"&gt;</p>
            <p> &lt;/form&gt;</p>
            <p> &lt;/body&gt;</p>
            <p> &lt;/html&gt;</p>

    <h3>Answer:</h3>

         <!DOCTYPE html>
         <html>
         <body>
         <h4>Text Input</h4>
         <form>
         First name:<br>
         <input type="text" name="firstname">
         <br>
         Last name:<br>
         <input type="text" name="lastname">
         </form>
         </body>
         </html>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="footer" runat="server">
    <h3>Notes:</h3>
     
    <p> It is possible to use the :valid and :invalid CSS pseudo-classes to style a &lt;form&gt; element based on whether or not the individual elements
        within the form are valid. </p>
    <p>The form element wraps a section of the document that contains form controls.</p>
    <p>The action attribute specifies the web address of a program that processes the information submitted via the form. </p>
    <p>The method attribute specifies the HTTP method that the browser should use to submit the form, such as POST or GET. Forms cannot be nested inside one anothe</p>

</asp:Content>
