﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApplication.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <h1>Web Programming</h1>

    <p> Web programming refers to the writing, markup and coding involved in Web development, which includes Web content, 
        Web client and server scripting and network security.</p>
            
    <p> To know more <a runat="server" href="~/WebProgramming"> click here </a></p>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="sidebar1" runat="server">
    <h1>Degital design</h1>
 
        <p>Digital designers combine their creative skills with technical know-how to bring graphics, animations and other visual effects to life.
            Digital design is the method by which graphic designs are created using computers, tablets, digital drawing tools for print,
            Web, television, electronic devices, and other media of innumerable nature and varieties.

        </p>
            <p> To know more <a runat="server" href="~/DegitalDesign"> click here </a></p>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="sidebar2" runat="server">

    <h1>Database Design</h1>

    <p>Database design is the organisation of data according to a database model. The designer determines what data must be stored and how the data elements interrelate. 
        With this information, they can begin to fit the data to the database model.Database design involves classifying data and identifying interrelationships.

    </p>

     <p> To know more <a runat="server" href="~/DatabaseDesign"> click here </a></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="footer" runat="server">

</asp:Content>



