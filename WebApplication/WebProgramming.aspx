﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="WebProgramming.aspx.cs" Inherits="WebApplication.WebProgramming" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

         <h2> Arrays </h2>
    <p>The JavaScript Array object is a global object that is used in the construction of arrays; which are high-level, list-like objects.</p>
    <p>  An array is a special variable, which can hold more than one value at a time.</p>
    <p>If you have a list of items (a list of car names, for example), storing the cars in single variables could look like this:</p> 
    <p>    var car1 = "Saab";</p>
    <p>    var car2 = "Volvo";</p>
    <p>    var car3 = "BMW";</p>
    <p>    Using an array literal is the easiest way to create a JavaScript Array.</p>
    <p>    Syntax:        </p>
    <p>    var array_name = [item1, item2, ...];  </p>        
    <p>    Example</p>
    <p>    var cars = ["Saab", "Volvo", "BMW"];</p>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="sidebar1" runat="server">
  
      <h2>Links</h2>
    <p><a href="https://www.w3schools.com/js/js_arrays.asp">W3Schools</a></p>
    <p><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array">Developer.mozilla</a></p>
    <p><a href="https://javascript.info/array">Javascript.info</a></p>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="sidebar2" runat="server">
    <h2>Example</h2>
        <p>&lt;!DOCTYPE html&gt;</p>
        <p>&lt;html&gt;         </p>
        <p>&lt;body&gt;         </p>
        <p>&lt;h2&gt;JavaScript Arrays&lt;/h2&gt;</p>
        <p>&lt;p id="demo"&gt;&lt;/p&gt;</p>
        <p>                             </p>
        <p>&lt;script&gt;               </p>
        <p>var cars = [                 </p>
        <p>    "Saab",                  </p>
        <p>    "Volvo",                 </p>
        <p>    "BMW"                    </p>
        <p>];                           </p>
        <p>document.getElementById("demo").innerHTML = cars;</p>
        <p>&lt;/script&gt;                                  </p>
        <p>&lt;/body&gt;                                    </p>
        <p>&lt;/html&gt;                                    </p>

    <h3>Answer:</h3>

    <!DOCTYPE html>
        <html>
        <body>
        
        <h4>JavaScript Arrays</h4>
        
        <p id="demo"></p>
        
        <script>
        var cars = [
            "Saab",
            "Volvo",
            "BMW"
        ];
        document.getElementById("demo").innerHTML = cars;
        </script>
        
        </body>
        </html>
    
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="footer" runat="server">
    <h2>Notes:</h2>

    <p>With JavaScript, the full array can be accessed by referring to the array name</p>
    <p>Arrays are Objects</p>
    <p>Arrays are a special type of objects. The typeof operator in JavaScript returns "object" for arrays.</p>
    <p>But, JavaScript arrays are best described as arrays.</p>
    <p>Arrays use numbers to access its "elements".</p>

</asp:Content>